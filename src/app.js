import React, { Fragment, useEffect } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";

// import Home from "./routes/Home/Home";
import Developer from "./routes/Developer/Developer";
import Repository from "./routes/Repository/Repository";
import { useDispatch } from "react-redux";
import { storeDevData, storeRepoData } from "./redux/actions";

function App () {
  const dispatch = useDispatch();
  useEffect(()=>{
    fetch("https://gh-trending-api.herokuapp.com/repositories")
      .then((res) => res.json())
      .then((result) => {
        dispatch(storeRepoData(result));
      })
      .catch((error) => console.log("error", error));

      fetch("https://gh-trending-api.herokuapp.com/developers")
      .then((res) => res.json())
      .then((result) => {
        dispatch(storeDevData(result));
      })
      .catch((error) => console.log("error", error));
  },[])
  return(
  <Fragment>
    <Router history={createBrowserHistory()}>
      <Switch>
        <Route exact path="/">
            <Redirect to="/repository" />
        </Route>
        <Route exact path="/repository" component={Repository} />
        <Route exact path="/developer" component={Developer} />
      </Switch>
    </Router>
  </Fragment>
  )
};

export default App
