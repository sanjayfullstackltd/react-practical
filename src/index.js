import "@babel/polyfill";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App  from "./app";
import store from "./redux/store";
import "./assets/index.scss";

ReactDOM.render(
<Provider store={store}>
    <App />
</Provider>

,
document.getElementById("App"));
